<?php
declare(strict_types = 1);

namespace Jerrt\Test;

use Elasticsearch\Client;
use Elasticsearch\ClientBuilder;
use Exception;

class easyEs
{
	/**配置*/
	private $config = [
        'node1'  => '127.0.0.1:9200',
    ];

	/**Client链接*/
	private $client;

	/**索引名*/
	private $index;

	/**type名*/
	private $type = '_doc';

	/**查询字段*/
	private $field;

	/**查询模式*/
	private $searchMode;

	/**查询模式的条件*/
	private $searchModeQuery;

	/**bool查询体临时存放*/
	private $temporaryStorage;

	/**高亮数据*/
	private $highlightData = [];

	/**排序数据*/
	private $sortData = [];

	/**聚合查询数据*/
	private $aggsData = [];

	/**分页*/
	private $from = null;//开始查询位置
	private $size = null;//获取数据条数


    public function __construct(array $config = [])
    {
        if(empty($config)) $this->config = $config;

        return $this->client();
    }

	/**
	 * 连接Elasticsearch
	 * Elasticsearch版本：7.6.2
	 * ik分词器版本：7.6.2
	 * @return Client
	 */
	public function client(): Client
    {
		try {
			if (empty($this->client)) {
				$this->client = ClientBuilder::create()->setHosts($this->config)->build();
			}
		} catch (Exception $exception) {
            throw new ($exception->getMessage());
		}
		return $this->client;
	}

	/**
	 * 创建索引
	 * @param $params array
	 * @return array
	 */
	public function createIndex(array $params): array
	{
		return $this->client()->indices()->create($params);
	}

	/**
	 * 删除索引
	 * @param $index_name array
	 * @return array
	 */
	public function deleteIndex(array $index_name): array
	{
		$params = array(
			'index' => $index_name,
		);
		return $this->client()->indices()->delete($params);
	}

	/**
	 * 修改索引信息
	 * @param $params array
	 * @return array
	 */
	public function putSettings(array $params): array
	{
		return $this->client()->indices()->putSettings($params);
	}

	/**
	 * 获取索引信息
	 * @param $index_list array
	 * @return array
	 */
	public function getSettings(array $index_list): array
	{
		$params = array(
			'index' => $index_list,
		);
		return $this->client()->indices()->getSettings($params);
	}

	/**
	 * 修改索引字段映射
	 * @param $params array
	 * @return array
	 */
	public function putMapping(array $params): array
	{
		return $this->client()->indices()->putMapping($params);
	}

	/**
	 * 获取索引字段映射
	 * @param $index_list array 索引名数组，为空时查询所有索引的映射
	 * @return array
	 */
	public function getMapping(array $index_list = []): array
	{
		$params = array(
			'index' => $index_list,
		);
		return $this->client()->indices()->getMapping($params);
	}



/***************************************************************原生操作***************************************************************/
	/**
	 * 添加一条数据
	 * @param array $params
	 * @return array
	 * @throws Exception
	 */
	public function addDoc(array $params = []): array
	{
		return $this->client()->index($params);
	}

	/**
	 * 批量操作
	 * @param $params array
	 * @return array
	 */
	public function bulk(array $params): array
	{
		return $this->client()->bulk($params);
	}

	/**
	 * 创建文档
	 * @param $params array
	 * @return array
	 */
	public function create(array $params): array
	{
		return $this->client()->create($params);
	}

	/**
	 * 更新文档
	 * @param $params array
	 * @return array
	 */
	public function update(array $params): array
	{
		return $this->client()->update($params);
	}

	/**
	 * 根据条件更新文档
	 * @param array $params
	 * @return array
	 */
	public function updateByQuery(array $params): array
	{
		return $this->client()->updateByQuery($params);
	}

	/**
	 * 删除文档
	 * @param array $params
	 * @return array
	 */
	public function delete(array $params): array
	{
		return $this->client()->delete($params);
	}

	/**
	 * 根据条件删除文档
	 * @param array $params
	 * @return array
	 */
	public function deleteByQuery(array $params): array
	{
		return $this->client()->deleteByQuery($params);
	}

	/**
	 * 根据id获取文档
	 * @param $params array
	 * @return array
	 */
	public function get(array $params): array
	{
		return $this->client()->get($params);
	}

	/**
	 * 搜索文档
	 * @param array $params
	 * @return array
	 */
	public function search(array $params): array
	{
		/*$params = array(
			'index' => 'demo',
			'type' => '_doc',
			'body' => array(
				// query 查询
				'query' => array(
					'match' => array(
						'title' => '科技',
					),
				),
				// 高亮
				'highlight' => array(
					"pre_tags" => ["<font color='red'>"],
					"post_tags" => ["</font>"],
					'fields' => array(
						'title' => new \stdClass(),
					),
				),
			),
			'from' => 1,
			'size' => 2,
		);*/
		return $this->client()->search($params);
	}



	/***************************************************************链式操作***************************************************************/
	/**
	 * 指定索引
	 *
	 * @param string $indexName
	 *
	 * @return easyEs
	 * @throws Exception
	*/
	public function index(string $indexName): easyEs
	{
		$this->checkIndex($indexName);
		$this->index = $indexName;
		return $this;
	}

	/**
	 * 指定type
	 *
	 * @param string $typeName
     *
     * @return easyEs
	 * @throws Exception
	 */
	public function type(string $typeName): easyEs
	{
		$this->checkType($typeName);
		$this->type = $typeName;
		return $this;
	}

	/**
	 * 条件 作废
	 *
	 * @param string       $searchMode 搜索方式【match | match_phrase | multi_match | term | terms | 等...】
	 * @param array|string $where
     *
     * @return easyEs
	 */
	/*public function where(string $searchMode, $where): Es
	{
		$this->searchMode = $searchMode;
		$this->where = $where;
		return $this;
	}*/

	/**
	 * 查询字段
	 *
	 * @param array|string $fieldName
	 *
	 * @return easyEs
	 */
	public function field($fieldName): easyEs
	{
		if(is_string($fieldName)) $fieldName = explode(',', $fieldName);

		$this->field = $fieldName;

		return $this;
	}

	/**
	 * 添加一条文档
	 * @param array $data ['字段'=>值]
	 * @return array
	 * @throws Exception
	 */
	public function insertDoc(array $data = []): array
	{
		$params = [
			'index' => $this->index,
			'type'  => $this->type,
		];

		$this->checkData($data);

		if(isset($data['id'])) {
			$params['id'] = $data['id'];
			unset($data['id']);
		}

		$params['body'] = $data;

		return $this->client()->index($params);
	}

	/**
	 * 根据id删除文档
	 * @param string|int $id
	 * @return array
	 * @throws Exception
	 */
	public function deleteDocById($id): array
	{
		$this->checkId($id);

		$params = [
			'index' => $this->index,
			'type'  => $this->type,
			'id'    => $id,
		];

		return $this->client()->delete($params);
	}

	/**
	 * 根据条件删除文档
	 * @return array
	 */
	public function deleteDocByQuery(): array
	{
		$params = [
			'index' => $this->index,
			'type' => $this->type,
			'body' => [
				'query' => [
					$this->searchMode => $this->searchModeQuery,
				],
			],
		];

		return $this->client()->deleteByQuery($params);
	}
	/*public function BACK_UP_deleteDocByQuery(): array
	{
		$params = [
			'index' => $this->index,
			'type' => $this->type,
			'body' => [
				'query' => [
					$this->searchMode => $this->where,
				],
			],
		];

		return $this->client()->deleteByQuery($params);
	}*/

	/**
	 * 根据id更新文档
	 * @param string|int $id
	 * @param array $data [字段 => 值]
	 * @return array
	 * @throws Exception
	 */
	public function updateDocById($id, array $data): array
	{
		$params = [
			'index' => $this->index,
			'type' => $this->type,
			'id'    => $id,
			'body'  => [
				'doc'  => $data
			]
		];
		return $this->client()->update($params);
	}

	/**
	 * 执行条件更新操作
	 * @param string $source
	 * @return array
	 */
	public function updateQuery(string $source): array
	{
		$params = [
			'index' => $this->index,
			'type' => $this->type,
			'body' => [
				'query' => [
					$this->searchMode => $this->searchModeQuery,
				],
				'script' => [
					'source' => $source,
				],
			],
		];
		//dd($params);

		return $this->client()->updateByQuery($params);
	}

	/**
	 * 根据条件更新文档
	 * @param array $data [字段 => 值]
	 * @return array
	 * @throws Exception
	 */
	public function updateDocByQuery(array $data): array
	{
		$source = '';
		foreach ($data as $k => $v){
			$source.= 'ctx._source.' . $k . '=' . $v . ';';
		}

		return $this->updateQuery($source);
	}

	/**
	 * 根据条件将(一个或多个)字段自增
	 * @param array $data [字段 => 自增值]
	 * @return array
	 * @throws Exception
	 */
	public function inc(array $data): array
	{
		$source = '';
		foreach ($data as $k => $v){
			$source.= 'ctx._source.' . $k . '+=' . $v . ';';
		}

		return $this->updateQuery($source);
	}

	/**
	 * 根据条件将(一个或多个)字段自减
	 * @param array $data [字段 => 自减值]
	 * @return array
	 * @throws Exception
	 */
	public function dec(array $data): array
	{
		$source = '';
		foreach ($data as $k => $v){
			$source.= 'ctx._source.' . $k . '-=' . $v . ';';
		}

		return $this->updateQuery($source);
	}

	/**
	 * 根据id获取文档
	 * @param string|int $id
	 * @return array
	 * @throws Exception
	*/
	public function getById($id): array
	{
		$this->checkId($id);

		$params = [
			'index' => $this->index,
			'type'  => $this->type,
			'id'    => $id,
		];

		return $this->client()->get($params);
	}

	/**
	 * 搜索方式：全文搜索
	 *
	 * @param array $data [查询字段 => 匹配值]
	 *
	 * @return easyEs
	*/
    public function match(array $data): easyEs
	{
		//如果是bool查询
		if($this->searchMode === 'bool') {

			$this->temporaryStorage[]['match'] = $data;
		}
		else{

			$this->searchMode = 'match';
			$this->searchModeQuery = $data;
		}

		return $this;
	}

	/**
	 * 搜索方式：段落搜索
	 *
	 * @param array $data [查询字段 => 匹配值]
	 *
	 * @return easyEs
     */
    public function matchPhrase(array $data): easyEs
	{
		//如果是bool查询
		if($this->searchMode === 'bool') {

			$this->temporaryStorage[]['match_phrase'] = $data;
		}
		else{
			$this->searchMode = 'match_phrase';
			$this->searchModeQuery = $data;
		}

		return $this;
	}

	/**
	 * 搜索方式：多字段匹配搜索
	 *
	 * @param string $query 查询的内容
	 * @param array  $fields 查询的字段[字段1, 字段2, ...]
	 *
	 * @return easyEs
	*/
	public function multiMatch(string $query, array $fields): easyEs
	{
		//如果是bool查询
		if($this->searchMode === 'bool') {
			$this->temporaryStorage[]['multi_match'] = [
				'query'     => $query,
				'fields'    => $fields
			];
		}
		else{

			$this->searchMode = 'multi_match';

			$this->searchModeQuery = [
				'query'     => $query,
				'fields'    => $fields
			];
		}

		return $this;
	}

	/**
	 * 搜索方式：精准搜索
     *
     * @param array $data [查询字段 => 匹配值]
     *
     * @return easyEs
	 */
	public function term(array $data): easyEs
	{
		//如果是bool查询
		if($this->searchMode === 'bool') {

			$this->temporaryStorage[]['term'] = $data;
		}
		else{

			$this->searchMode = 'term';
			$this->searchModeQuery = $data;
		}

		return $this;
	}

	/**
	 * 搜索方式：多关键字精准搜索
	 *
	 * @param string $field 查询字段
	 * @param array  $data [匹配值1, 匹配值2, ...]
	 *
	 * @return easyEs
	 */
	public function terms(string $field, array $data): easyEs
	{
		//如果是bool查询
		if($this->searchMode === 'bool') {

			$this->temporaryStorage[]['terms'] = [$field => $data];
		}
		else{

			$this->searchMode = 'terms';
			$this->searchModeQuery = [$field => $data];
		}

		return $this;
	}

	/**
	 * 搜索方式：range范围搜索
	 *
	 * @param array $data ['字段1'=>[[符号,值],[符号,值],...], '字段2'=>[[符号,值],[符号,值],...], ...]
	 *
	 * @return easyEs
	 * @throws Exception
	 */
	public function range(array $data): easyEs
	{
		//符号需要转换
		$punctuation    = [
			'>'     => 'gt',
			'>='    => 'gte',
			'<'     => 'lt',
			'<='    => 'lte',
		];

		//符号不需要转换
		$original = ['gt', 'gte', 'lt', 'lte',];

		$range_list = [];

		foreach ($data as $key => $value){

			foreach ($value as $v){

				//检查每个子数组中值的数量
				if(count($v) != 2) throw new Exception('Needed 2 values but gave '. count($v));

				//检查符号是否合规
				if(!isset($punctuation[$v[0]]) && !in_array($v[0],$original)){

					throw new Exception($v[0] . 'is an unknown symbol');

				}

				//进行符号转换
				if(isset($punctuation[$v[0]])){
					$range_list[$key][$punctuation[$v[0]]] =  $v[1];
				}else{
					$range_list[$key][$v[0]] =  $v[1];
				}
			}
		}

		//如果是bool查询
		if($this->searchMode === 'bool') {

			$this->temporaryStorage[]['range'] = $range_list;
		}
		else{

			$this->searchMode = 'range';
			$this->searchModeQuery = $range_list;
		}

		return $this;
	}

	/**
	 *bool组合查询
	 *
	 * @param string $boolMode 语句【must、must_not、should】
	 * @param array|callable $boolModeQuery 仅支持数组和回调方法
	 *
	 * @return easyEs
     * @throws Exception
	 */
	public function bool(string $boolMode, $boolModeQuery): easyEs
	{

		$this->searchMode = 'bool';

		//是数组
		if(is_array($boolModeQuery)){
			/**
			 * $boolModeQuery 为array时 例子
			[
				'match' => [
					'age'  => '18'
				],
				'term' => [
					'desc'  => '广州市'
				],
				'range' => [
					'age' => [
						'lte' => 20,
					]
				]
			]
			 */
			$boolModeQueryList = [];
			foreach ($boolModeQuery as $k => $v){
				$boolModeQueryList[][$k] = $v;
			}
			$this->searchModeQuery = [
				$boolMode   => $boolModeQueryList
			];
		}
		//是回调方法
		elseif (is_callable($boolModeQuery)){
			call_user_func($boolModeQuery,$this);

			$this->searchModeQuery[$boolMode] = $this->temporaryStorage;

			$this->temporaryStorage = [];

		}
		else{
			throw new Exception('Only supported array and callable');
		}

		return $this;
	}

	/**
	 * 高亮
	 *
	 * @param array|string $field 需要高亮的字段
	 * @param array        $pre_tags 开始标签
	 * @param array        $post_tags 结束标签
	 *
	 * @return easyEs
	 */
	public function highlight($field, array $pre_tags = ["<font color='red'>"], array $post_tags = ["</font>"]): easyEs
	{
		if(is_string($field)){
			$field = explode(',',$field);
		}

		$field_list = [];
		foreach ($field as $v){
			$field_list[$v] = new \stdClass();
		}

		$this->highlightData = [
			"pre_tags"  => $pre_tags,
			"post_tags" => $post_tags,
			"field"     => $field_list,
		];

		return $this;
	}

    /**
     * 排序
	 *
	 * @param array $data ['字段1'=>'asc | desc', '字段2'=>'asc | desc', ...]
	 *
	 * @return easyEs
	 */
	public function sort(array $data): easyEs
	{
		$this->sortData = [];

		foreach ($data as $k => $v){
			$this->sortData[] = [
				$k => [
					'order' => $v
				]
			];
		}
        return $this;
	}

	/**
	 * 分页
	 *
	 * @param int $page 页数
     * @param int $listRows 每页数量
	 *
	 * @return easyEs
	 */
	public function page(int $page, int $listRows): easyEs
	{
		if($page === 0) $page = 1;

		$this->from = ($page - 1) * $listRows;
		$this->size = $listRows;

		return $this;
    }

	/**
	 * 指定起始位置
	 * @param int $offset 起始位置
	 * @return easyEs
	 */
	public function from(int $offset): easyEs
	{
		$this->from = $offset;
		return $this;
    }

	/**
	 * 指定查询数量
	 * @param int $length 查询数量
	 * @return easyEs
	 */
	public function size(int $length): easyEs
	{
		$this->size = $length;
		return $this;
	}

	/**
	 * 最大值
	 * @param string $field 字段
	 * @return $this
	 */
	public function max(string $field): easyEs
	{
		$this->aggsData[$field.'max'] = [
			'max' => [
				'field' => $field
			]
		];

		return $this;
	}

	/**
	 * 最小值
	 * @param string $field 字段
	 * @return $this
	 */
	public function min(string $field): easyEs
	{
		$this->aggsData[$field.'min'] = [
			'min' => [
				'field' => $field
			]
		];

		return $this;
	}

	/**
	 * 总和
	 * @param string $field 字段
	 * @return $this
	 */
	public function sum(string $field): easyEs
	{
		$this->aggsData[$field.'sum'] = [
			'sum' => [
				'field' => $field
			]
		];

		return $this;
	}

	/**
	 * 平均值
	 * @param string $field 字段
	 * @return $this
	 */
	public function avg(string $field): easyEs
	{
		$this->aggsData[$field.'avg'] = [
			'avg' => [
				'field' => $field
			]
		];

		return $this;
	}

	/**
	 * 计算各种聚合 count,max,min,sum,avg
	 *
	 * @param string $field 字段
	 *
	 * @return $this
	 */
	public function stats(string $field): easyEs
	{
		$this->aggsData[$field.'stats'] = [
			'stats' => [
				'field' => $field
			]
		];

		return $this;
    }

    /**
	 * 字段值不重复数量
	 * @param string $field 字段
	 * @return $this
	 */
	public function cardinality(string $field): easyEs
	{
		$this->aggsData[$field.'cardinality'] = [
			'cardinality' => [
				'field' => $field
			]
		];

		return $this;
	}

	/**
	 * 统计在指定字段(整列)中，相同的值在总数量中的占比，例如有：【10,20,30,40,10】 10在此列中的占比为40%
	 *
	 * @param string $field     字段
     * @param array $percents   值
	 *
	 * @return $this
	 */
	public function percentiles(string $field, array $percents = []): easyEs
	{
		$this->aggsData[$field.'percentiles'] = [
			'percentiles' => [
				'field' => $field
			]
		];

		if($percents) $this->aggsData[$field.'percentiles']['percentiles']['percents'] = $percents;

		return $this;
	}

	/**
	 * 计算给定的一个或多个值中(数组)，在某一字段(整列)中总数量的占比
	 *
	 * @param string $field 字段
     * @param array  $values 值
	 *
	 * @return $this
	 */
	public function percentileRanks(string $field, array $values = []): easyEs
	{
		$this->aggsData[$field.'percentiles'] = [
			'percentile_ranks' => [
				'field' => $field,
				'values'=> $values
			]
		];

		return $this;
	}

	/**
     * 分组【只分组，不对数据进行操作，分组后统计该字段相同值的数量】
	 *
	 * @param string $field 分组字段
	 *
	 * @return $this
	 */
	public function onlyGroup(string $field): easyEs
	{

		$this->aggsData[$field.'cardinality'] = [
			'terms' => [
				'field' => $field
			]
		];

		return $this;
	}

	/**
	 * 分组
	 *
	 * @param string $field 分组字段
	 * @param int    $size 每组获取数据条数，默认=全部
	 * @param array  $sort 分组排序：['排序字段' => 'asc|desc']，默认不排序
	 *
	 * @return $this
	 */
	public function group(string $field, int $size = 0, array $sort = []): easyEs
	{
		$this->aggsData[$field.'cardinality'] = [
			'terms' => [
				'field' => $field,
				'aggs'  => [//子聚合
					'get_top_'.$field   => [
						'top_hits'      => new \stdClass()
					]
				]
			]
		];

		if($size > 0 || $sort) $this->aggsData[$field.'cardinality']['terms']['aggs']['get_top_'.$field]['top_hits'] = [];

		if($size > 0) $this->aggsData[$field.'cardinality']['terms']['aggs']['get_top_'.$field]['top_hits']['size'] = $size;
		if($sort) {
			$this->aggsData[$field.'cardinality']['terms']['aggs']['get_top_'.$field]['top_hits']['sort'] = [
				[
					$sort[0] => [
						'order' => $sort[1]
					]
				]
			];
		}

		return $this;
	}

	/**
	 * 根据条件获取文档
	 */
	public function getByQuery()
	{
		$params = [
			'index' => $this->index,
			'body'  => []
		];

        if($this->searchMode && $this->searchModeQuery){
            $params['body']['query'] = [
                $this->searchMode => $this->searchModeQuery
            ];
        }

		//查询指定字段
		if($this->field) $params['_source'] = $this->field;

		//高亮
		if($this->highlightData) $params['body']['highlight'] = $this->highlightData;

		//排序
		if($this->sortData) $params['body']['sort'] = $this->sortData;

		//聚合
		if($this->aggsData) $params['body']['aggs'] = $this->aggsData;

		//分页
		if($this->from !== null) $params['body']['from'] = $this->from;
		if($this->size !== null) $params['body']['size'] = $this->size;

		return $this->client()->search($params);
	}

	/**
	 * 指定 index 和 type
	 *
     * @param string $index
	 * @param string $type
	 *
	 * @return $this
	 * @throws Exception
	 */
	public function name(string $index, string $type = '_doc'): easyEs
	{
		$this->checkIndex($index);
		$this->checkType($type);
		$this->index = $index;
		$this->type = $type;
		return $this;
	}

	/**
	 * 排序
	 * @param array $data ['字段1'=>'asc | desc', '字段2'=>'asc | desc', ...]
	 * @return easyEs
	 */
	public function order(array $data): easyEs
	{
		$this->sortData = [];

		foreach ($data as $k => $v){
			$this->sortData[] = [
				$k => [
					'order' => $v
				]
			];
		}
		return $this;
	}

	/**
     * 指定查询数量
     * @access public
	 *
	 * @param int $offset 起始位置
	 * @param int $length 查询数量
	 *
	 * @return $this
	 */
	public function limit(int $offset, int $length): easyEs
	{
		$this->from = $offset;
		$this->size = $length;
		return $this;
	}




	/**
	 * 检查索引
	 * @param string $index
	 * @throws Exception
	 */
	public function checkIndex(string $index){

		if (!$index) throw new Exception('"index" is required');//index为必须
	}

	/**
	 * 检查type
	 * @param string $type
	 * @throws Exception
	 */
	public function checkType(string $type){

		if (!$type) throw new Exception('"type" is required');//type为必须
	}

	/**
	 * 检查数据
	 * @param array $data
	 * @throws Exception
	*/
	public function checkData(array $data){

		if (!$data) throw new Exception('There is no data to add');//没有需要添加的数据
	}

	/**
	 * 检查id
	 * @param string|int $id
	 * @throws Exception
	*/
	public function checkId($id){

		if (!$id) throw new Exception('"id" is required');//id为必须
	}

	/**
	 * 检查索引 和 type
	 * @param string $index
	 * @param string $type
	 * @throws Exception
	 */
	public function checkIndexAndType(string $index, string $type)
	{
		$this->checkIndex($index);
		$this->checkType($type);
	}

	/**
	 * 检查索引 和 type 和 id
	 * @param string $index
	 * @param string $type
	 * @param string|int $id
	 * @throws Exception
	 */
	public function checkIndexAndTypeAndId(string $index, string $type, $id)
	{
		$this->checkIndex($index);
		$this->checkType($type);
		$this->checkId($id);
	}

	public function __call($name,$value)
	{
		print "函数11: $name(参数: ";
		print_r($value);
		echo ")不存在!<br/>";
	}

	public static function __callStatic($name,$value)
	{
		print "函数22: $name(参数: ";
		print_r($value);
		echo ")不存在!<br/>";
	}

}
